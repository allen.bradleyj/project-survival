from django.contrib import admin
from .models import Stat, AttributeStat, Weapon, Enemy, Adjective

@admin.register(Stat)
class StatAdmin(admin.ModelAdmin):
    list_display = (
        "character_name",
        "current_health",
        "max_health",
        "stamina",
        "warmth",
        "nourishment",
        "level",

    )

@admin.register(AttributeStat)
class AttributeStatadmin(admin.ModelAdmin):
    list_display = (
        "attack",
        "defense",
        "owner"

    )

@admin.register(Weapon)
class Weaponadmin(admin.ModelAdmin):
    list_display = (
        "name",
        "attack_stat",
        "owner",
    )


@admin.register(Enemy)
class Enemyadmin(admin.ModelAdmin):
    list_display = (
        "name",
        "attack_stat",
        "current_health",
        "max_health",
        "accuracy",
        "defense",
        "level"
    )

@admin.register(Adjective)
class Adjectiveadmin(admin.ModelAdmin):
    list_display = (
        'adjectives',
    )
