from django.db import models

class Stat(models.Model):
    character_name = models.CharField(max_length=40, null=True)
    current_health = models.IntegerField()
    max_health = models.IntegerField(null=True)
    stamina = models.IntegerField()
    warmth = models.IntegerField()
    nourishment = models.IntegerField()
    level = models.IntegerField()
    score = models.IntegerField(null=True)
    experience_needed = models.IntegerField(null=True)
    current_experience = models.IntegerField(null=True)

    def __str__(self):
        return self.character_name

class AttributeStat(models.Model):
    attack = models.IntegerField()
    defense = models.IntegerField()
    owner = models.ForeignKey(
        Stat, related_name="attributes", null=True, on_delete=models.PROTECT
    )

class Weapon(models.Model):
    name = models.CharField(max_length=20)
    attack_stat = models.IntegerField()
    owner = models.ForeignKey(
        Stat, related_name="weapon", null=True, on_delete=models.PROTECT
    )

class Enemy(models.Model):
    name = models.CharField(max_length=50)
    attack_stat = models.IntegerField()
    current_health = models.IntegerField()
    max_health = models.IntegerField()
    defense = models.IntegerField()
    accuracy = models.IntegerField()
    level = models.IntegerField()
    experience_given = models.IntegerField(null=True)

    def __str__(self):
        return self.name

class Adjective(models.Model):
    adjectives = models.CharField(max_length=255)

    def set_my_array(self, arr):
        self.adjectives = ','.join(arr)

    def get_my_array(self):
        return self.adjectives.split(',')
