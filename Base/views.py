from django.shortcuts import render, get_object_or_404, redirect
from .models import Stat,AttributeStat,Weapon, Enemy, Adjective
import random
import time
import math


def base_page(request):
    stats = Stat.objects.all
    context = {"stats" : stats}

    return render(request, "base/basestats.html", context)

def detail_page(request,id):
    character = get_object_or_404(Stat,id=id)
    enemy_id = request.session.get('enemy_id')
    attack = request.GET.get('attack', False)
    dead = False
    previous_enemy_id = request.session.get('enemy_id')
    previous_enemy = ""
    previous_experience = 0
    enemy = None
    level_one_enemies = Enemy.objects.filter(level=1).exclude(id=previous_enemy_id)
    enemy = random.choice(level_one_enemies)
    adjectives = Adjective.objects.order_by('?').first()
    adjective_array = adjectives.get_my_array()
    random_word = random.choice(adjective_array)
    percentage_to_level = (character.current_experience / character.experience_needed) * 100
    level_up = False



    if enemy_id:
        enemy = get_object_or_404(Enemy, id=enemy_id)
        dead = False


    if enemy.current_health <= 0:
        enemy.current_health = enemy.max_health
        previous_enemy = enemy.name
        previous_experience = math.ceil(((enemy.experience_given * character.level) * .5))
        character.current_experience += previous_experience


        if character.current_experience >= character.experience_needed:
            character.level += 1
            character.experience_needed = math.ceil(25 * (character.level+1 ** 2 ))
            character.current_experience = 0
            level_up = True


        percentage_to_level = (character.current_experience / character.experience_needed) * 100
        enemy.save()
        dead = True
        character.score += 1
        character.save()
        level_one_enemies = Enemy.objects.filter(level=1).exclude(id=previous_enemy_id)
        enemy = random.choice(level_one_enemies)
        request.session['enemy_id'] = enemy.id


    health_difference = character.max_health - character.current_health
    enemy_health_difference = enemy.max_health - enemy.current_health

    context = {
        "stats_object" : character,
        "health_difference" : health_difference,
        'enemy' : enemy,
        'enemy_health_difference':enemy_health_difference,
        'dead': dead,
        'previous_enemy': previous_enemy,
        'random_word' : random_word,
        'attack' : attack,
        'previous_experience':previous_experience,
        'percentage_to_level':percentage_to_level,
        'level_up' : level_up
    }

    return render(request, "base/details.html", context)


def update_health(request, id, enemy_id):

    health = get_object_or_404(Stat,id=id)
    enemy = get_object_or_404(Enemy,id=enemy_id)
    attributes = health.attributes.get()
    weapon = attributes.owner.weapon.get()
    pre_total_attack = (attributes.attack + weapon.attack_stat)
    total_attack = random.randint(pre_total_attack-5,pre_total_attack)
    damage = (total_attack - enemy.defense)

    if request.method == 'POST':
        if (enemy.attack_stat - attributes.defense) > 0 and enemy.current_health > (total_attack - enemy.defense):
            health.current_health -= enemy.attack_stat - attributes.defense

        if (total_attack - enemy.defense) > 0:
            enemy.current_health -=(total_attack - enemy.defense)

        health.save()
        enemy.save()
        time.sleep(.75)

        if request.POST.get('action') == 'attack':
            url = f'/base/{id}/?attack=True&damage={damage}'
            return redirect(url)

        return redirect(detail_page, id )

    context = {'health':health}

    return render(request, 'base/update_health.html', context)

def refresh(request, id):
    health = get_object_or_404(Stat,id=id)
    if request.method == 'POST':
        return redirect(detail_page, id )

    context = {'health':health}

    return render(request, 'base/update_health.html', context)

def revive(request,id):
    health = get_object_or_404(Stat,id=id)

    if request.method == 'POST':
        health.current_health = health.max_health
        health.score -= 1
        health.save()

    return redirect(detail_page,id)



def attribute_stats(request, id):
    attributes = get_object_or_404(AttributeStat,id=id)
    weapon = attributes.owner.weapon.get()
    total = attributes.attack + weapon.attack_stat
    context = {"attributes":attributes,
               "weapon":weapon,
               "total":total}

    return render(request,"base/attributes.html", context)
