from django.urls import path
from .views import base_page,update_health,detail_page,attribute_stats,refresh,revive

urlpatterns = [
    path("stats/", base_page, name= 'base_page'),
    path("<int:id>/<int:enemy_id>", update_health, name="update_health"),
    path("<int:id>/", detail_page, name="detail_page"),
    path("<int:id>/", refresh, name="refresh"),
    path("revive/<int:id>/", revive, name="revive"),
    path("attributes/<int:id>/", attribute_stats, name="attribute_stats"),
    path("", base_page, name="base_page"),

]
